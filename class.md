### classes[14] = "An intro to population genetics analyses' results interpretation"

#### Análise de Sequências Biológicas 2023

![Logo EST](assets/logo-ESTB.png)

Francisco Pina Martins

[@FPinaMartins](https://twitter.com/FPinaMartins)

---

### Summary

* &shy;<!-- .element: class="fragment" -->Evolution!
* &shy;<!-- .element: class="fragment" -->Allele frequencies
* &shy;<!-- .element: class="fragment" -->F-statistics
* &shy;<!-- .element: class="fragment" -->PCA plots
* &shy;<!-- .element: class="fragment" -->Admixture plots

---

### How does evolution work?

&shy;<!-- .element: class="fragment" style="float: right"-->![Darwin's Tree of life](assets/Darwin_TOL.jpg)

<ul>
<li class="fragment">Mechanisms</li>
  <ul>
  <li class="fragment">Mutation</li>
  <li class="fragment">Gene-flow</li>
  <li class="fragment">Genetic drift</li>
  <li class="fragment">Non-random mating</li>
  <li class="fragment">[Natural] Selection</li>
  </ul>
</ul>

<div class="fragment" style="float: bottom-left"><img src="assets/find_robin.png" alt="Robin with magnifyng glass" style="background:none; border:none; box-shadow:none;"/></div>

---

### Why is it important to understand?

* &shy;<!-- .element: class="fragment" -->Biodiversity
* &shy;<!-- .element: class="fragment" -->Species conservation
* &shy;<!-- .element: class="fragment" -->Response to change
* &shy;<!-- .element: class="fragment" -->As an algorithm

&shy;<!-- .element: class="fragment" -->![Biodiversity](assets/Biodiversity.jpg)

---

### Evolution at a fine scale

![Doge](assets/doge.png)

---

<section data-background="assets/forest2_map00.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map01.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map03.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map04.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map05.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map06.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map08.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map09.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map10.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map11.png" data-background-size="1336px" data-background-transition="none">

---

<section data-background="assets/forest2_map12.png" data-background-size="1336px" data-background-transition="none">

---

### F-Statistics

* &shy;<!-- .element: class="fragment" -->Introduced by S. Wright (1931), improved by [Weir & Cockerham (1984)](https://doi.org/10.1111/j.1558-5646.1984.tb05657.x)
* &shy;<!-- .element: class="fragment" -->Based on Heterozigosity (proportion of heterozygotes) measures
  * &shy;<!-- .element: class="fragment" -->**F<sub>IS</sub>**: Individual inbreeding relative to their subpopulation
    * &shy;<!-- .element: class="fragment" -->Higher values indicate **more** inbreeding
  * &shy;<!-- .element: class="fragment" -->**F<sub>ST</sub>**: Subpopulation inbreeding relative to the total
    * &shy;<!-- .element: class="fragment" -->Higher values indicate **more** subpopulation isolation
  * &shy;<!-- .element: class="fragment" -->**F<sub>IT</sub>**: Individual inbreeding relative to the total
    * &shy;<!-- .element: class="fragment" -->Higher values indicate **more** heterozygote deficiency

&shy;<!-- .element: class="fragment" -->![FST](assets/fst.png)

&shy;<!-- .element: class="fragment" -->[Learn more](https://www.nature.com/articles/nrg2611)

---

### Isolation by distance

* &shy;<!-- .element: class="fragment" -->Sometimes individuals of different subpopulations diverge simply due to geographic distance
* &shy;<!-- .element: class="fragment" -->Can be tested by compare pairwise F<sub>ST</sub> and geographic distance
  * &shy;<!-- .element: class="fragment" -->"Mantel test"
  * &shy;<!-- .element: class="fragment" -->A significant correlation **suggests** the existence of isolation by distance

&shy;<!-- .element: class="fragment" -->[![Mantel test](assets/mantel.png)](https://doi.org/10.1371%2Fjournal.pone.0020144)

|||

### Performing a mantel test

* &shy;<!-- .element: class="fragment" -->You can use [Genepop](https://kimura.univ-montp2.fr/~rousset/Genepop.htm)
  * &shy;<!-- .element: class="fragment" -->Also has an [`R` version](https://cran.r-project.org/package=genepop)
* &shy;<!-- .element: class="fragment" -->An (ancient) alternative is [Arlequin](http://cmpg.unibe.ch/software/arlequin35/)
  * &shy;<!-- .element: class="fragment" -->It has a particularly good manual, with detailed explanations of many population genetics concepts and theory, but as a program it is now very dated, slow, and bloated

&shy;<!-- .element: class="fragment" -->![Arlequin logo](assets/Arlequin_single.png)

---

### PCA in population genomics

* &shy;<!-- .element: class="fragment" -->Very common in population genetics analyses
* &shy;<!-- .element: class="fragment" -->Allow a comprehensive overview over complex, multidimensional datasets
* &shy;<!-- .element: class="fragment" -->Particularly useful when there is *a priori* knowledge on the expected groups

&shy;<!-- .element: class="fragment" -->![Penguin PCA](assets/penguin_pca.png)

|||

### Interpreting a PCA

* &shy;<!-- .element: class="fragment" -->An example from *Limonium* (simple)

&shy;<!-- .element: class="fragment" -->![Limonium PCA](assets/Lim_species_PCA.png)

|||

### Interpreting a PCA

* &shy;<!-- .element: class="fragment" -->An example from *Limonium* (complex)

&shy;<!-- .element: class="fragment" -->![Limonium vulgare PCA](assets/L_vulgare_PCA.png)

---

### Admixture plots

* &shy;<!-- .element: class="fragment" -->Bar plots, where each bar represents an individual
* &shy;<!-- .element: class="fragment" -->Each colour represents a different genetic cluster (AKA: **K**)
  * &shy;<!-- .element: class="fragment" -->The number of clusters is frequently a range of values
  * &shy;<!-- .element: class="fragment" -->There are plenty of *ad-hoc* ways to estimate K
    * &shy;<!-- .element: class="fragment" -->[Evanno's method](http://www.springerlink.com/content/jnn011511h415358/)
    * &shy;<!-- .element: class="fragment" -->[MavericK](https://doi.org/10.1534%2Fgenetics.115.180992)
* &shy;<!-- .element: class="fragment" -->Each individual is *partially* assigned to each genetic cluster

|||

### Interpreting admixture plots

* &shy;<!-- .element: class="fragment" -->An example from *Limonium* (simple)

&shy;<!-- .element: class="fragment" -->[![Limonium species ALSTRUCTURE](assets/Lim_species_ALSTRUCTURE_small.png)](assets/Lim_species_ALSTRUCTURE.png)

|||

### Interpreting admixture plots

* &shy;<!-- .element: class="fragment" -->An example from *Limonium* (complex)

&shy;<!-- .element: class="fragment" -->[![Limonium species ALSTRUCTURE](assets/L_vulgare_ALSTRUCTURE_small.png)](assets/L_vulgare_ALSTRUCTURE.png)

---

### References

* [An Interactive Introduction to Organismal and Molecular Biology, 2nd ed. (Biodiversity)](https://openbooks.lib.msu.edu/isb202/chapter/biodiversity/)
* [Holsinger & Weir 2009](https://doi.org/10.1038%2Fnrg2611)
* [Sean Maden Thesis](https://www.reed.edu/biology/professors/srenn/pages/research/2011_students/sean/SM_thesis.html)
* [Arlequin manual](http://cmpg.unibe.ch/software/arlequin35/man/Arlequin35.pdf)
* [Mantel test strengths and caveats](https://doi.org/10.1590/s1415-47572013000400002)
* [A first look at sea-lavenders genomics](https://doi.org/10.1186/s12870-022-03974-2)
* [Interpretation of admixture plots](https://dienekes.blogspot.com/2011/06/interpretation-of-admixture-results.html)
* [On over-interpretation of admixture plots](https://doi.org/10.1038/s41467-018-05257-7)
